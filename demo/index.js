import dataurl from "./dataurl.js";
var app = new Vue({
  el: '#app',
  data: {
    sheetDataHead: [],
    sheetDataBody: [],
    date: "",
    issueDate: "",
    printBackground: true,
    loading: false
  },
  methods: {
    gSheetProcessor(options, callback, onError) {
      const {apiKey, sheetId, sheetName, sheetNumber, returnAllResults, filter, filterOptions} = options

      if(!options.apiKey || options.apiKey === undefined) {
        throw new Error('Missing Sheets API key');
      }

      return this.gsheetsAPI({
        apiKey,
        sheetId,
        sheetName,
        sheetNumber
      })
      .then(result => {
        callback(result.values);
      })
      .catch(err => onError(err.message));
    },
    gsheetsAPI({apiKey, sheetId, sheetName, sheetNumber = 1}) {
      try {
        const sheetNameStr = sheetName && sheetName !== '' ? encodeURIComponent(sheetName) : `Sheet${sheetNumber}`
        const sheetsUrl = `https://sheets.googleapis.com/v4/spreadsheets/${sheetId}/values/${sheetNameStr}?dateTimeRenderOption=FORMATTED_STRING&majorDimension=ROWS&valueRenderOption=FORMATTED_VALUE&key=${apiKey}`;

        return fetch(sheetsUrl)
          .then(response => {
            if (!response.ok) {
              console.log('there is an error in the gsheets response');
              throw new Error('Error fetching GSheet');
            }
            return response.json();
          })
          .then(data => data)
          .catch(err => {
            throw new Error(
              'Failed to fetch from GSheets API. Check your Sheet Id and the public availability of your GSheet.'
            );
          });
      } catch (err) {
        throw new Error(`General error when fetching GSheet: ${err}`);
      }
    },
    matchValues(valToMatch, valToMatchAgainst, matchingType) {
      try {
        if (typeof valToMatch != 'undefined') {
          valToMatch = valToMatch.toLowerCase().trim();
          valToMatchAgainst = valToMatchAgainst.toLowerCase().trim();

          if (matchingType === 'strict') {
            return valToMatch === valToMatchAgainst;
          }

          if (matchingType === 'loose') {
            return (
              valToMatch.includes(valToMatchAgainst) ||
              valToMatch == valToMatchAgainst
            );
          }
        }
      } catch (e) {
        console.log(`error in matchValues: ${e.message}`);
        return false;
      }

      return false;
    },
    filterResults(resultsToFilter, filter, options) {
      let filteredData = [];

      // now we have a list of rows, we can filter by various things
      return resultsToFilter.filter(item => {

        // item data shape
        // item = {
        //   'Module Name': 'name of module',
        //   ...
        //   Department: 'Computer science'
        // }

        let addRow = null;
        let filterMatches = [];

        if (
          typeof item === 'undefined' ||
          item.length <= 0 ||
          Object.keys(item).length <= 0
        ) {
          return false;
        }

        Object.keys(filter).forEach(key => {
          const filterValue = filter[key]; // e.g. 'archaeology'

          // need to find a matching item object key in case of case differences
          const itemKey = Object.keys(item).find(thisKey => thisKey.toLowerCase().trim() === key.toLowerCase().trim());
          const itemValue = item[itemKey]; // e.g. 'department' or 'undefined'

          filterMatches.push(
            matchValues(itemValue, filterValue, options.matching || 'loose')
          );
        });

        if (options.operator === 'or') {
          addRow = filterMatches.some(match => match === true);
        }

        if (options.operator === 'and') {
          addRow = filterMatches.every(match => match === true);
        }

        return addRow;
      });
    },
    processGSheetResults(
      JSONResponse,
      returnAllResults,
      filter,
      filterOptions
    ) {
      const data = JSONResponse.values;
      const startRow = 1; // skip the header row(1), don't need it

      let processedResults = [{}];
      let colNames = {};

      for (let i = 0; i < data.length; i++) {
        // Rows
        const thisRow = data[i];

        for (let j = 0; j < thisRow.length; j++) {
          // Columns/cells
          const cellValue = thisRow[j];
          const colNameToAdd = colNames[j]; // this will be undefined on the first pass

          if (i < startRow) {
            colNames[j] = cellValue;
            continue; // skip the header row
          }

          if (typeof processedResults[i] === 'undefined') {
            processedResults[i] = {};
          }

          if (typeof colNameToAdd !== 'undefined' && colNameToAdd.length > 0) {
            processedResults[i][colNameToAdd] = cellValue;
          }
        }
      }

      // make sure we're only returning valid, filled data items
      processedResults = processedResults.filter(
        result => Object.keys(result).length
      );

      // if we're not filtering, then return all results
      if (returnAllResults || !filter) {
        return processedResults;
      }

      return filterResults(processedResults, filter, filterOptions);
    },
    formatTodaysDate() {
      const now = new Date();
      const year = now.getFullYear();
      const month = now.getMonth() + 1;
      const day = now.getDate();
      return [day.toString().padStart(2, '0'), month.toString().padStart(2, '0'),  year].join('-');
    },
    getTodaysDateFrom(inTimestamp) {
      const date = new Date(inTimestamp);
      if(date) {
        return [date.getDate().toString().padStart(2, '0'), (date.getMonth() + 1).toString().padStart(2, '0'), date.getFullYear()].join('-');
      }
      formatTodaysDate();
    },
    centeredText(doc, text, y) {
      var textWidth = doc.getStringUnitWidth(text) * doc.internal.getFontSize() / doc.internal.scaleFactor;
      var textOffset = (doc.internal.pageSize.width - textWidth) / 2;
      doc.text(textOffset, y, text);
    },
    printPDF(inData) {
      this.loading = true;
      const dataObj = {
        "groomName": inData[1],
        "groomAddress1": `S/O ${inData[2]}`,
        "groomAddress2": inData[3],
        "groomAddress3": inData[4],
        "brideName": inData[5],
        "brideAddress1": `D/O ${inData[6]}`,
        "brideAddress2": inData[7],
        "brideAddress3": inData[8],
        "registerNumber": inData[9],
        "date": ((this.date == "") ? this.formatTodaysDate() : this.getTodaysDateFrom(this.date)),
        "issueDate": ((this.issueDate == "") ? this.formatTodaysDate() : this.getTodaysDateFrom(this.issueDate)),
        "printBackground": this.printBackground !== (null || undefined) ? this.printBackground : true
      };
      console.log(dataObj);
      var doc = new jsPDF();
      const width = doc.internal.pageSize.getWidth();
      const height = doc.internal.pageSize.getHeight();
      if(dataObj.printBackground) {
        doc.addImage(dataurl, 'JPEG', 0, 0, width, height);
      }
      this.centeredText(doc, "AND", height/2)
      this.centeredText(doc, dataObj.groomAddress3, height/2 - 9)
      this.centeredText(doc, dataObj.groomAddress2, height/2 - 15)
      this.centeredText(doc, dataObj.groomAddress1, height/2 - 21)
      this.centeredText(doc, dataObj.brideAddress1, height/2 + 16)
      this.centeredText(doc, dataObj.brideAddress2, height/2 + 22)
      this.centeredText(doc, dataObj.brideAddress3, height/2 + 28)
      doc.setFontSize(12);
      this.centeredText(doc, dataObj.date, height/2 + 67)
      this.centeredText(doc, dataObj.registerNumber, height/2 + 82)
      doc.setFontSize(10);
      doc.text(32, height/2 + 108, dataObj.issueDate)
      doc.setFontType("bold");
      doc.setFontSize(24);
      this.centeredText(doc, dataObj.groomName, height/2 - 28)
      this.centeredText(doc, dataObj.brideName, height/2 + 9)
      doc.setProperties({
        title: `${dataObj.groomName}_${dataObj.brideName}_${dataObj.date}.pdf`
      });
      doc.output('dataurlnewwindow')
      this.loading = false;
    }
  },
  mounted() {
    this.loading = true;
    const TOKEN = 'AIzaSyCPWw14a1DsMvVfaGX02bSP-NDZG5ThDoI';

    // test sheet id, Sheets API key, and valid auth scope
    const demoSheetId = '1OHbIXS3y8lfG4P1ByVJeV43-SjTOiJ8qzIrdfgiRYzY';
    const apiKey = TOKEN;

    const options = {
      apiKey: apiKey,
      sheetId: demoSheetId,
      sheetNumber: 1,
      returnAllResults: false,
      filter: {
        department: 'archaeology',
        'module description': 'introduction'
      },
      filterOptions: {
        operator: 'or',
        matching: 'loose',
      }
    };

    this.gSheetProcessor(
      options,
      results => {
        let sheetDataHeadArray = results.filter((arr, index) => index === 0);
        if(sheetDataHeadArray && sheetDataHeadArray.length > 0) {
          this.sheetDataHead = sheetDataHeadArray[0];
        }

        let sheetDataBodyArray = results.filter((arr, index) => index !== 0);
        if(sheetDataBodyArray && sheetDataBodyArray.length > 0) {
          this.sheetDataBody = sheetDataBodyArray;
        }

        this.loading = false;
      },
      error => {
        console.log('error from sheets API', error);
        const main = document.querySelector('#output');
        main.innerHTML = `Error while fetching sheets: ${error}`;
      }
    );
  }
})
